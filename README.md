# Tote-Bet

## Prerequisites
 * Node.js Version 8 or higher [It might work on lower versions however, only tested in version from 8 and higher]
    
## Installation
  If you have nodejs installed all it require is to pull the repository.

## Running the app
 * To start the app. in the console run the command:
 ```node app.js'```
 * Alternately if you have npm you can simply run the command: 
 ```npm start```
 
 * Once the app is started you can start giving the inputs line by line.
 
 * To exit the app type stop any time.
 
 