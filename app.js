const constants = require('./src/constants')
const Helper =require('./src/helper');
const Win =  require('./src/win');
const Exacta = require('./src/exacta');
const Place = require('./src/place');

const helper = new Helper();
const win = new Win();
const exacta = new Exacta();
const place = new Place();

const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
})
console.log('Tote-Bet app. Enter your bets/result');
  readline.on(`line`, (input) => {
     const inputArr = input.split(':');
     if (input == "stop") {
         readline.close()
     }
     if (helper.isValid(inputArr)) {
        const action = inputArr[0];
        if (action === constants.bet) {
            const product = inputArr[1];
            const selectedItem =inputArr[2]
            const amount = Number(inputArr[3]);
            if (product === constants.win) {
                win.addToPool(selectedItem, amount);
            }
            if (product === constants.place) {
                place.addToPool(selectedItem, amount);
            }
            if (product === constants.exacta) {
                exacta.addToPool(selectedItem, amount);
            }
        }
        if (action === constants.result) {
            const first = inputArr[1];
            const second = inputArr[2];
            const third = inputArr[3];
            const winIndividualBetSum = win.getIndividualBetSum();
            const placeIndividualBetSum = place.getIndividualBetSum();
            const exactaIndividualBetSum = exacta.getIndividualBetSum();
            const winDividend = helper.calculateDividend(first, winIndividualBetSum, constants.win);
            const placeDividendFirst = helper.calculateDividend(first, placeIndividualBetSum, constants.place);
            const placeDividendSecond = helper.calculateDividend(second, placeIndividualBetSum, constants.place);
            const placeDividendThird = helper.calculateDividend(third, placeIndividualBetSum, constants.place);
            const exactaDividend = helper.calculateDividend(`${first},${second}`, exactaIndividualBetSum, constants.bet)

            console.log(`Win:${first}:$${winDividend}`)
            console.log(`Place:${first}:$${placeDividendFirst}`)
            console.log(`Place:${second}:$${placeDividendSecond}`)
            console.log(`Place:${third}:$${placeDividendThird}`)
            console.log(`Exacta:${first},${second}:$${exactaDividend}`)
            readline.close()
        }
     } else {
          console.log('try again or type "stop" to exist');
     }

  })
