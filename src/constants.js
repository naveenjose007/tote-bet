module.exports = {
    bet: 'Bet',
    result: 'Result',
    win: 'W',
    place: 'P',
    exacta: 'E',
    winCommission: 15,
    placeCommission: 12,
    exactaCommission: 18
}