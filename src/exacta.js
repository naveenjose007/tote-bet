class Exacta {
    constructor() {
        this.individualBetSum = {}
    }

    addToPool (selectedItem, amount) {
        if (this.individualBetSum[selectedItem]) {
            this.individualBetSum[selectedItem] += amount
        } else {
            this.individualBetSum[selectedItem] = amount

        }
    }

    getIndividualBetSum () {
        return this.individualBetSum;
    }
}

module.exports = Exacta