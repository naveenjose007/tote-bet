const constants = require('./constants')

class Helper {
    isValid(input) {
        if (input.length != 4) {
            console.log('incomplete input')
            return false;
        }
        if (input[0] !== constants.bet && input[0] !== constants.result) {
            console.log(`invalid action. Action should be ${constants.bet} or ${constants.result}`)
            return false;
        }
        if (input[0] === constants.bet) {
            if (![constants.win, constants.place, constants.exacta].includes(input[1])) {
                console.log(`invalid bet option. Action should be ${constants.win} or ${constants.place} or ${constants.exacta}`)
                return false;
            }
            if (input[1] === constants.exacta) {
                const exactaPicks = input[2].split(',');
                if (exactaPicks.length !== 2) {
                    console.log('you should choose 2 values')
                    return false;
                }
            }
            if (isNaN(input[3])) {
                console.log('invalid bet amount');
                return false
            }

        }
        return true;
    }

    calculateDividend (winner, individualBetSum, product) {
        const betSumArr = Object.values(individualBetSum)
        if (!betSumArr.length) {
            return 0;
        }
        const totalPool = betSumArr.reduce(this.add)
        let commission = 0;
        if (product === constants.win) {
            commission = constants.winCommission
        }
        if (product === constants.place) {
            commission = constants.placeCommission
        }
        if (product === constants.exacta) {
            commission = constants.exactaCommission
        }
        const netAmount = totalPool - totalPool * commission / 100;
        const winnerSumAmount = individualBetSum[winner];

        if (!winnerSumAmount) {
            return 0;
        }
        let dividend = netAmount / winnerSumAmount;
        if (product === constants.place) {
            dividend = dividend / 3;
        }
        return  dividend.toFixed(2);
    }

    add (total, number ) {
        return total + number;
    }

}

module.exports = Helper;